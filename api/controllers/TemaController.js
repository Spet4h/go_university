const Tema = require( '../models/tema');
const Proceso = require('../models/proceso')
// const mongoose = require('mongoose');

const cloudinary = require('cloudinary');
require('dotenv').config();
require('../config/cloudinaryConfig_v_2')

const findModel = require('./modules/findDataById')


let getTemas = (req,res) =>{
  Tema.find()
    .populate([
        {
        path: 'usuario',
       },
       {
        path: 'temas.proceso',
        populate:{
         path:'area' 
        }
       }

])
     
     .exec((err,tema)=>{
         if(err){
             res.status(500).send({
                 message: `Error ${err}`
             })
         }
         if(!tema){
             res.status(404).send({
                 message:'No existen etiquetas'
             })
         }
         
         res.json(tema)
     })
}
let getTemaUser = (req,res)=>{
 
  let {id_user}=req.params; 
  findModel.getDataById(req,res,model = {
      name:Tema,
      field:{'usuario':{'_id':id_user}},
      populate:[{ path: 'proceso' },{path:'area'}]
  })
}
let getTemaByIdAndUser = (req,res)=>{
   let { id_tema,id_user } = req.params
    Tema.find({_id:id_tema},{usuario:{_id:id_user}})
       .populate('usuario')
       .populate({ path: 'proceso' })
       .exec((err,tema)=>{
        if(err){
            res.status(500).json({
                message:`Error al mostrar el tema ${err}`
            })
        }
        if(!tema){
            res.status(404).json({
                message:'No existe el usuario'
            })
        }
        
        res.json(tema)
    })
  }

  let getTemaByUserRole = (req,res)=>{
      
    let role = req.params.role
 
    
   
    Tema.find()
       .populate([{path:'usuario'},{path:'area'},{path:'proceso'}])
       .exec((err,tema)=>{
        if(err){
            res.status(500).json({
                message:`Error al mostrar el tema ${err}`
            })
        }
        if(!tema){
            res.status(404).json({
                message:'No existe el usuario'
            })
        }
        const topic =  tema.filter(item => item.usuario.role === role)
        res.json(topic)
    })
  }


let getTemaAreaProceso = (req,res)=>{

  let {id_area,id_work,role} = req.params

  

  if(id_area === 'all' & id_work !== 'all'){
 
    Tema.find({proceso:id_work})
       .populate([{path:'usuario'},{path:'area'},{path:'proceso'}])
       .exec((err,tema)=>{
           if(err){
               res.status(500).send({
                   message: `Error ${err}`
               })
           }
           if(!tema){
               res.status(404).send({
                   message:'No existen etiquetas'
               })
           }
           
            
            
           const topic =  tema.filter(item => item.usuario.role === role)
           res.json(topic)
       })
  };
  if(id_work === 'all' && id_area !=='all'){
  
    Tema.find({area:{_id:id_area}})
         .populate({path:'proceso',populate:{path:'area'}})
         .populate({path:'area'})
         .populate('usuario')
         .exec((err,tema)=>{
             if(err){
                 res.status(500).send({
                     message: `Error ${err}`
                 })
             }
             if(!tema){
                 res.status(404).send({
                     message:'No existen etiquetas'
                 })
             }
           
             const topic =  tema.filter(item => item.usuario.role === role)
             res.json(topic)
         })
            
       
      
  };
  if(id_work !== 'all' && id_area !=='all'){
    Tema.find({proceso:{_id:id_work,area:{_id:id_area}}})
       .populate([{path: 'usuario'},
       {path:'proceso',populate:{path:'area'} }
    ]
       )
       .exec((err,tema)=>{
           if(err){
               res.status(500).send({
                   message: `Error ${err}`
               })
           }
           if(!tema){
               res.status(404).send({
                   message:'No existen etiquetas'
               })
           }
           const topic =  tema.filter(item => item.usuario.role === role)
           res.json(topic)
       })
  };
  if(id_work === 'all' && id_area ==='all'){
    Tema.find()
       .populate([{path: 'usuario'},
       {path:'proceso',populate:{path:'area'} }
    ]
       )
       .exec((err,tema)=>{
        if(err){
            res.status(500).send({
                message: `Error ${err}`
            })
        }
        if(!tema){
            res.status(404).send({
                message:'No existen etiquetas'
            })
        }
        const topic =  tema.filter(item => item.usuario.role === role)
        res.json(topic)
       })
  }

}



let addTema= async(req,res)=>{
    const body = req.body;
    //const id = body.id_user
   
//    const url = []
//    let url = []
    // for(let file in req.files){ 
    //  const result = await cloudinary.uploader.upload_v2(req.files[file].path)
    //   url.push(result.secure_url)
    // }

    //save image cloudinary services
    const result = await cloudinary.uploader.upload(req.file.path)
  
    
    const tema = new Tema({
        usuario:body.usuario,
        titulo:body.titulo,
        descripcion:body.descripcion,
        area:body.area,
        proceso:body.proceso,
        photo:result.secure_url, 
        ranking:body.ranking,
       
  
      })
   
      tema.save((err,tema)=>{
        if(err){
            res.status(500).send({
                message:`Error ${err}`
            })
        }
        res.send(tema);
      }) 

  }

  let postImages = async (req,res) =>{
    
      
    const result = await cloudinary.uploader.upload(req.file.path)
    res.json({url_image:result.secure_url})
  }

module.exports={
  addTema,
  getTemaAreaProceso,
  getTemas,
  getTemaUser,
  getTemaByIdAndUser,
  getTemaByUserRole,
  postImages
}
