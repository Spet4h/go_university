const express=require('express'); 
const aporteController = require('../controllers/AporteController') ;
/*============================================
    Importamos el archivo de authentication
=========================================== */

// Importamos el archivo para verficar rol admin;
const router=express.Router(); 

router.post('/aporte',aporteController.addAporte),
router.post('/aporte/:id/comentario',aporteController.aporteComentario)
router.get('/aportes',aporteController.getAportes)
router.get('/aporte/usuario/:id',aporteController.getAporteByUser)
router.get('/aporte/:id/comentarios',aporteController.getPostComments)
router.get('/aporte/:id_area/:id_work',aporteController.getAporteProceso)


module.exports=router; 